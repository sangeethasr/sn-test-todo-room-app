package com.example.windows.todo_test_application.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.windows.todo_test_application.Activities.AddActivity;
import com.example.windows.todo_test_application.Activities.MainActivity;
import com.example.windows.todo_test_application.Adapters.NotesAdapter;
import com.example.windows.todo_test_application.Models.TodoDB;
import com.example.windows.todo_test_application.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotesFragment extends Fragment implements View.OnClickListener {
    RecyclerView notesRecycleView;
    LinearLayoutManager mLinearLayoutManager;
    ArrayList todoArrayList;
    TodoDB db;
    NotesAdapter mNotesAdapter;
    FloatingActionButton fab;

    public NotesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View notesView = inflater.inflate(R.layout.fragment_notes, container, false);
        mLinearLayoutManager = new LinearLayoutManager(notesView.getContext(),LinearLayoutManager.VERTICAL,false);
        notesRecycleView = notesView.findViewById(R.id.notes_frag_RV);
        notesRecycleView.setLayoutManager(mLinearLayoutManager);

        db = new TodoDB(notesView.getContext());
        todoArrayList = db.getReminder();
        mNotesAdapter = new NotesAdapter();
        mNotesAdapter.setTodoArrayList(todoArrayList);
        notesRecycleView.setAdapter(mNotesAdapter);

        fab = notesView.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(this);
        return notesView;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == fab.getId())
        {
            Intent intent = new Intent(this.getContext(), AddActivity.class);
            startActivity(intent);
        }
    }
}
