package com.example.windows.todo_test_application.Models;

import java.io.Serializable;

public class ArcModels implements Serializable{
    private String arcId;
    private String arcDate;
    private String arcDescription;

    public String getArcId() {
        return arcId;
    }

    public void setArcId(String arcId) {
        this.arcId = arcId;
    }

    public String getArcDate() {
        return arcDate;
    }

    public void setArcDate(String arcDate) {
        this.arcDate = arcDate;
    }

    public String getArcDescription() {
        return arcDescription;
    }

    public void setArcDescription(String arcDescription) {
        this.arcDescription = arcDescription;
    }
}
