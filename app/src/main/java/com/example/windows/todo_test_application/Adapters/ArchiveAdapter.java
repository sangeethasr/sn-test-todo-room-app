package com.example.windows.todo_test_application.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.windows.todo_test_application.Models.ArcModels;
import com.example.windows.todo_test_application.R;
import java.util.ArrayList;


public class ArchiveAdapter extends RecyclerView.Adapter<ArchiveAdapter.ArchiveViewHolder> {

    ArrayList<ArcModels> ArchiveArray;

    public void setArchiveArray(ArrayList<ArcModels> archiveArray) {
        ArchiveArray = archiveArray;
    }


    ArchiveViewHolder mArchiveViewHolder;
    @Override
    public ArchiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_arc_item,parent,false);
        return new ArchiveViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ArchiveViewHolder holder, int position) {
        ArcModels model = ArchiveArray.get(position);
        holder.archiveDateTxt.setText(model.getArcDate());
        holder.archiveDescriptionTxt.setText(model.getArcDescription());
    }

    @Override
    public int getItemCount() {
        return ArchiveArray.size();
    }

    public class ArchiveViewHolder extends RecyclerView.ViewHolder{

        TextView archiveDateTxt;
        TextView archiveDescriptionTxt;
        public ArchiveViewHolder(View itemView) {
            super(itemView);
            archiveDateTxt = itemView.findViewById(R.id.todo_item_date_txt);
            archiveDescriptionTxt = itemView.findViewById(R.id.todo_item_des_txt);
        }
    }
}
