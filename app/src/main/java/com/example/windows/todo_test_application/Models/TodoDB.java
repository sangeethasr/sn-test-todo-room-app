package com.example.windows.todo_test_application.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class TodoDB extends SQLiteOpenHelper {

    public TodoDB(Context context) {
        super(context, TodoConstants.DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TodoTable.NoteTable.CREATE_TABLE);
        db.execSQL(TodoTable.ArcTable.CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TodoTable.NoteTable.DROP_TABLE);
        db.execSQL(TodoTable.ArcTable.DROP_TABLE);

        onCreate(db);
    }

    public long addReminder(TodoModels model) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TodoTable.NoteTable.TODO_ID, model.getTodoId());
        contentValues.put(TodoTable.NoteTable.TODO_DATE, model.getDate());
        contentValues.put(TodoTable.NoteTable.TODO_DESCRIPTION, model.getDescription());
        long id = db.insert(TodoTable.NoteTable.TODO_TABLE, null, contentValues);
        return id;
    }

    public long changeNotePosition(TodoModels model) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TodoTable.ArcTable.TODO_ID, model.getTodoId());
        contentValues.put(TodoTable.ArcTable.TODO_DATE, model.getDate());
        contentValues.put(TodoTable.ArcTable.TODO_DESCRIPTION, model.getDescription());
        long id = db.insert(TodoTable.ArcTable.TODO_TABLE, null, contentValues);
        return id;

    }

    public ArrayList<ArcModels> getArcArrayList() {

        ArrayList<ArcModels> arcArrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TodoTable.ArcTable.TODO_TABLE, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                ArcModels model = new ArcModels();
                model.setArcDate(cursor.getString(cursor.getColumnIndex(TodoTable.ArcTable.TODO_DATE)));
                model.setArcDescription(cursor.getString(cursor.getColumnIndex(TodoTable.ArcTable.TODO_DESCRIPTION)));
                model.setArcId(cursor.getString(cursor.getColumnIndex(TodoTable.ArcTable.TODO_ID)));
                arcArrayList.add(model);
            }
        }
        return arcArrayList;

    }

    public ArrayList<TodoModels> getReminder() {
        ArrayList<TodoModels> arrayTodo = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TodoTable.NoteTable.TODO_TABLE, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                TodoModels model = new TodoModels();
                model.setDate(cursor.getString(cursor.getColumnIndex(TodoTable.NoteTable.TODO_DATE)));
                model.setDescription(cursor.getString(cursor.getColumnIndex(TodoTable.NoteTable.TODO_DESCRIPTION)));
                model.setTodoId(cursor.getString(cursor.getColumnIndex(TodoTable.NoteTable.TODO_ID)));
                arrayTodo.add(model);
            }
        }
        return arrayTodo;
    }
}