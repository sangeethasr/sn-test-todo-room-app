package com.example.windows.todo_test_application.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.windows.todo_test_application.Adapters.ArchiveAdapter;
import com.example.windows.todo_test_application.Models.ArcModels;
import com.example.windows.todo_test_application.Models.TodoDB;
import com.example.windows.todo_test_application.Models.TodoModels;
import com.example.windows.todo_test_application.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArchiveFragment extends Fragment {
    RecyclerView archiveRecycleView;
    LinearLayoutManager mLinearLayoutManager;
    TodoDB db;
    ArrayList<ArcModels> ArchiveArray;
    ArchiveAdapter mArchiveAdapter;

    public ArchiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View archiveView = inflater.inflate(R.layout.fragment_archive, container, false);
        archiveRecycleView = archiveView.findViewById(R.id.archive_frag_RV);
        mLinearLayoutManager = new LinearLayoutManager(archiveView.getContext(),LinearLayoutManager.VERTICAL,false);
        archiveRecycleView.setLayoutManager(mLinearLayoutManager);

        db = new TodoDB(archiveView.getContext());
        ArchiveArray = db.getArcArrayList();
        mArchiveAdapter = new ArchiveAdapter();
        mArchiveAdapter.setArchiveArray(ArchiveArray);
        archiveRecycleView.setAdapter(mArchiveAdapter);

        return archiveView;
    }

}
