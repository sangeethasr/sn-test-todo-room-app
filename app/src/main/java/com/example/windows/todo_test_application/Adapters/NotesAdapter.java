package com.example.windows.todo_test_application.Adapters;

import android.app.Activity;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.windows.todo_test_application.Activities.MainActivity;
import com.example.windows.todo_test_application.Models.TodoDB;
import com.example.windows.todo_test_application.Models.TodoModels;
import com.example.windows.todo_test_application.R;

import java.util.ArrayList;
import java.util.List;


public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesViewHolder> {

    ArrayList<TodoModels> todoArrayList;

    public void setTodoArrayList(ArrayList<TodoModels> todoArrayList) {
        this.todoArrayList = todoArrayList;
    }

    @Override
    public NotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_item, parent, false);
        return new NotesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotesViewHolder holder, int position) {
        TodoModels models = todoArrayList.get(position);
        holder.dateAadapter.setText(models.getDate());
        holder.descriptionAdapter.setText(models.getDescription());

    }

    @Override
    public int getItemCount() {
        return todoArrayList.size();
    }

    public class NotesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView dateAadapter;
        TextView descriptionAdapter;
        Button deleteButton;
        long check;

        public NotesViewHolder(View itemView) {
            super(itemView);
            dateAadapter = itemView.findViewById(R.id.todo_item_date_txt);
            descriptionAdapter = itemView.findViewById(R.id.todo_item_des_txt);
            deleteButton = itemView.findViewById(R.id.todo_item_delete_button);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == deleteButton.getId()) {
                TodoModels model = new TodoModels();
                model.setDate(dateAadapter.getText().toString());
                model.setDescription(descriptionAdapter.getText().toString());
                TodoDB database = new TodoDB(itemView.getContext());
                check = database.changeNotePosition(model);
            }
        }
    }
}
