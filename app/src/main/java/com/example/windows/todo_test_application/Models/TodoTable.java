package com.example.windows.todo_test_application.Models;

public class TodoTable {

    public class NoteTable {
        public static final String TODO_ID = "todoId";
        public static final String TODO_TABLE = "notes_table";
        public static final String TODO_DATE = "date";
        public static final String TODO_DESCRIPTION = "description";
        public static final String CREATE_TABLE = "create table " + TODO_TABLE + " ( " + TODO_ID +
                " integer primarykey auto increment , " + TODO_DATE + " text , " + TODO_DESCRIPTION +
                " text )";
        public static final String DROP_TABLE = "drop table if exists " + TODO_TABLE;

    }

    public class ArcTable {

        public static final String TODO_ID = "arcId";
        public static final String TODO_TABLE = "arc_table";
        public static final String TODO_DATE = "arcDate";
        public static final String TODO_DESCRIPTION = "arcDescription";
        public static final String CREATE_TABLE = "create table " + TODO_TABLE + " ( " + TODO_ID +
                " integer primarykey auto increment , " + TODO_DATE + " text , " + TODO_DESCRIPTION +
                " text )";
        public static final String DROP_TABLE = "drop table if exists " + CREATE_TABLE;
    }
}

