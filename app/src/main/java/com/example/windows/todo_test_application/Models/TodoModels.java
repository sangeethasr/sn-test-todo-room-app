package com.example.windows.todo_test_application.Models;

import java.io.Serializable;

public class TodoModels implements Serializable{
    private String todoId;
    private String date;
    private String description;

    public String getTodoId() {
        return todoId;
    }

    public void setTodoId(String todoId) {
        this.todoId = todoId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
