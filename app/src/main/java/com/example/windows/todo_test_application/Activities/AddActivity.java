package com.example.windows.todo_test_application.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windows.todo_test_application.Models.TodoDB;
import com.example.windows.todo_test_application.Models.TodoModels;
import com.example.windows.todo_test_application.R;

public class AddActivity extends AppCompatActivity {
    EditText dateEtxt;
    EditText descriptionEtxt;
    Button saveButton;
    TodoDB db;
    long check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        dateEtxt = findViewById(R.id.todo_add_date_txt);
        descriptionEtxt = findViewById(R.id.todo_add_des_txt);
        saveButton = findViewById(R.id.todo_add_save_btn);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.getId() == saveButton.getId()) {

                    TodoModels model = new TodoModels();
                    model.setDate(dateEtxt.getText().toString());
                    model.setDescription(descriptionEtxt.getText().toString());
                    db = new TodoDB(AddActivity.this);
                    check = db.addReminder(model);
                    if (check == -1) {
                        Toast.makeText(AddActivity.this, "Can't save", Toast.LENGTH_SHORT).show();
                    } else if (check != -1) {
                        Toast.makeText(AddActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
